---
title: System Information
layout: default
parent: Modules
---

# {{ page.title }}

## Commands

- **Fetch host machine/server information.**
[Syntax: `info`]

  Provides basic system information about the computer/machine hosting your userbot.
<!--stackedit_data:
eyJoaXN0b3J5IjpbMzg0MTIzMTg0XX0=
-->
